import groovy.json.JsonOutput

node('master') {
    def server = params.host
    def cret ='tomcat_private_key'

    def command_map =  ["tomcat_path":"ls /home/tomcat | grep apache-tomcat-8.0.47 | wc -l",
                        "logs_path":"ls /home/tomcat/logs | wc -l",
                        "jdk_ver":"java -version 2>&1 >/dev/null | grep 'java version' | awk '{print \$3}' | sed 's/\"//g' | sed 's/1\\.\\([0-9]*\\)\\(.*\\)/\\1/; 1q'",
                        "tomcat_pid":"ps -ef | grep tomcat | grep -v grep | wc -l"
                        ]

    def result_map = ["tomcat_path":0,
                      "logs_path":0,
                      "jdk_ver":0,
                      "tomcat_pid":0]

    inspectTest(cret,server,command_map,result_map)
}

def inspectTest(cret,server,command_map,result_map){
    stage('SSH Inspect'){
        sshagent (credentials: [cret]) {
            command_map.each{k,v->
                def ret = sh(script: "ssh -o StrictHostKeyChecking=no tomcat@${server} ${command_map.get(k)}",returnStdout: true).trim()
                println("ret: " + ret)
                result_map.put(k,ret)
            }
            result_map.put("serverIp",server)
            result_map.put("buildId",BUILD_ID)
            notifyDeployCentral(result_map)
        }
    }
}

def notifyDeployCentral(result_map){
    stage("InspectNotify DeployCentral"){
        echo "InspectNotify DeployCentral: "+ result_map
        def json_data = JsonOutput.toJson(result_map)
        def response = httpRequest(url: "${params.callBackHost}/deploy-central/prod/jenkins/inspectNotify",
                                   httpMode: 'POST',
                                   acceptType: 'APPLICATION_JSON',
                                   contentType: 'APPLICATION_JSON',
                                   requestBody: json_data,
                                   consoleLogResponseBody: true,
                                   timeout: 10)
        println('Status: '+response.status)
    }
}