node('master') {
    // modify node_name and ip address fields 取消[全局安全设置]-[防止跨站点请求伪造]//
    // ---------------------------------------//
    def cret ='jenkins_private_key'

    try{
        wrap([$class: 'MaskPasswordsBuildWrapper',
              varPasswordPairs: [[password: "${params.jenkins_user}", var: 'jenkins_user'],
                                 [password: "${params.jenkins_pwd}", var: 'jenkins_pwd']]]){
            stage('Delete Jenkins slave') {
                deleteNode(params.nodeLabel);
            }
            stage('Delete Remote slave') {
                deleteFileToNode(params.host);
            }

            stage('Notify DeployCentral'){
                status = 30081 //删除服务器成功
                notifyDeployCentral(status)
            }
        }
    }catch (exc) {
        echo 'Delete Server Error !'
        status = 30080 //删除服务器失败
        notifyDeployCentral(status)
        throw exc
    }

}

def execurl(jenkins_user,jenkins_pwd,groovy_script){
    sh "curl --user '${jenkins_user}:${jenkins_pwd}' --data-urlencode  'script=${groovy_script}' -X POST '${jenkins_url}'/scriptText"
}

def deleteNode(nodeName){
    def groovy_script = """
       import hudson.model.Node.Mode
       import hudson.slaves.*
       import jenkins.model.Jenkins
       Node node = Jenkins.instance.getNode(\"${nodeName}\")
       println("---- Shutting down node ----!");
       node.getComputer().setTemporarilyOffline(true,null);
       node.getComputer().doDoDelete();
       """
    execurl(params.jenkins_user,params.jenkins_pwd,groovy_script);
}

def deleteFileToNode(host){
    sh """
       echo kill \\`ps -ef \\| grep jenkins-agent \\| grep -v grep \\|  awk \\\'{print \\\$2}\\\'\\` > stop.sh
       cat stop.sh
       chmod 755 stop.sh
       ssh -o StrictHostKeyChecking=no ${host} < \"stop.sh\"
     """
}

def deleteOfflineNode(nodeName){
    def groovy_script = """
            for (Node aSlave in Jenkins.instance.getNodes()) {
            println("");
            println("==========");
            println("Name: " + aSlave.name);
            println("computer.isOffline: " + aSlave.getComputer().isOffline());
            println("computer.getLog: " + aSlave.getComputer().getLog());
            println("computer.getBuilds: " + aSlave.getComputer().getBuilds());
            if (aSlave.getComputer().isOffline()) {
              println("----- Shutting down node -----!");
              aSlave.getComputer().setTemporarilyOffline(true,null);
              aSlave.getComputer().doDoDelete();
            }
        }
       """
    execurl(params.jenkins_user,params.jenkins_pwd,groovy_script);
}


def notifyDeployCentral(status){
    stage("Notify DeployCentral"){
        echo "Notify DeployCentral: "+status
        def response = httpRequest(url:"${params.callBackHost}/deploy-central/prod/jenkins/notify?callBackId=${params.callBackId}&status=${status}&buildId=${BUILD_ID}&action=${JOB_NAME}@${params.nodeLabel}",timeout: 10)
        println('Status: '+response.status)
    }
}
