node('master') {
    def server = params.server

    def command_map =  ["tomcat_path":"ls /home/tomcat | grep apache-tomcat-8.0.47 | wc -l",
                        "logs_path":"ls /home/tomcat/logs | wc -l",
                        "jdk_ver":"java -version 2>&1 >/dev/null | grep 'java version' | awk '{print \$3}' | sed 's/\"//g' | sed 's/1\\.\\([0-9]*\\)\\(.*\\)/\\1/; 1q'",
                        "tomcat_pid":"ps -ef | grep tomcat | grep -v grep | wc -l"
                        ]

    def result_map = ["tomcat_path":0,
                      "logs_path":0,
                      "jdk_ver":0,
                      "tomcat_pid":0]

    inspectTest('jenkins_private_key',server,command_map,result_map)
}

def inspectTest(cret,server,command_map,result_map){
    stage('SSH Inspect'){
        sshagent (credentials: [cret]) {
            command_map.each{k,v->
                def ret = sh(script: "ssh -o StrictHostKeyChecking=no ${server} ${command_map.get(k)}",returnStdout: true).trim()
                println("ret: " + ret)
                result_map.put(k,ret)
            }
        }
    }
}

def notifyDeployCentral(status){
    stage("Notify DeployCentral"){
        echo "Notify DeployCentral: "+status
        def response = httpRequest(url:"${params.callBackHost}/deploy-central/prod/jenkins/notify?callBackId=${params.callBackId}&status=${status}&buildId=${BUILD_ID}&action=${JOB_NAME}",timeout: 10)
        println('Status: '+response.status)
    }
}