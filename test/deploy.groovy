#!groovy
import groovy.json.JsonOutput
import groovy.json.JsonSlurper



node (params.nodeLabel) {
    def status = 0
    def tomcatHome = '/home/tomcat'
    def tomcatWebPath = tomcatHome + '/webapps'
    def bakPath = '/opt/app-bak'
    println(params.action)
    if('deploy' == params.action){
        download(status)
        deployPackage(tomcatHome,tomcatWebPath,bakPath,status)
        verifyApp(status)
    }else {
        println('No Action Not Found...')
    }
}

def download(status){
    stage('Pull Artifact') {
        try {
            echo " Pull Artifact ${params.app}-${params.tag} PackageName: ${params.packageName} "
            sh """
              curl ${params.nexusRepos}/${params.app}/${params.tag}/${params.app}-${params.tag}.${params.packageType} -o ${params.packageName}.${params.packageType}  
           """
            status = 30011 //拉取成功
            notifyDeployCentral(status)
        }catch (exc) {
            echo 'Pull Artifact error !'
            status = 30010 //拉取失败
            notifyDeployCentral(status)
            throw exc
        }
    }
}

def bakArtifactForWar(tomcatWebPath,bakPath){
    stage('Bak Artifact') {
        try {
            echo "Bak Artifact"
            def warCount = sh(script: "ls -l ${tomcatWebPath} | grep war |wc -l",returnStdout: true).trim()
            println("warCount: "+warCount)
            if((warCount as Integer) > 0){
                sh "ls ${tomcatWebPath} | grep war > bakResult"
                def bakByte = sh(script: 'ls -l | grep bakResult | awk \'{print $5}\'',returnStdout: true).trim()
                if((bakByte as Integer) > 0){
                    def bakResults = readFile('bakResult').split("\r?\n")
                    sh "mkdir -p ${bakPath}"
                    bakResults.each {
                        sh """
                        cp ${tomcatWebPath}/${it} ${bakPath}
                    """
                    }
                    sh "ls ${bakPath}"
                }
            }
        }catch (e){
            echo 'BakArtifact Error !'
            throw e
        }
    }
}

def deployPackage(tomcatHome,tomcatWebPath,bakPath,status){
    stage('Deploy Package'){
        try {
            echo "Deploy Package"
            if ('war' == params.packageType){
                stopTomcat()
                bakArtifactForWar(tomcatWebPath,bakPath)
                copyWarToTomcat(tomcatWebPath)
                startTomcat(tomcatHome)
            }else if('jar' == params.packageType){
                //stopApp("${params.app}")
                println(" start app ... ")
            }
            status = 30051 //部署成功
            notifyDeployCentral(status)
        }catch (exc) {
            echo 'Deploy Artifact error !'
            status = 30050 //部署失败
            notifyDeployCentral(status)
            throw exc
        }
    }
}

def verifyApp(status) {
    stage('Verify App'){
        try{
            sleep 20
            echo "check done"
            def ips = sh(script: "ip addr | grep 'scope global' | awk '{print \$2}'",returnStdout: true).trim()
            println ips
            def ip = ips.substring(0,ips.indexOf("/"))
            println ip
            def resp = httpRequest(url:"http://${ip}:${params.appPort}${params.appCheckUrl}",timeout: 90)
            println('Status: '+resp.status)
            status = 30061 //部署失败
            notifyDeployCentral(status)
        }catch (exc) {
            echo 'Verify Artifact error !'
            status = 30060 //部署失败
            notifyDeployCentral(status)
            throw exc
        }
    }
}

def notifyDeployCentral(status){
    stage("Notify DeployCentral"){
        echo "Notify DeployCentral: "+status
        def response = httpRequest(url:"${params.callBackHost}/deploy-central/prod/jenkins/notify?callBackId=${params.callBackId}&status=${status}&buildId=${BUILD_ID}&action=${JOB_NAME}@${params.nodeLabel}",timeout: 10)
        println('Status: '+response.status)
    }
}


def stopTomcat(){
    println("---- Stop Tomcat ----")
    def pid = sh(script: 'ps -ef | grep -w tomcat | grep -v grep | awk \'{print $2}\'',returnStdout: true).trim()
    echo 'Tomcat pid: '+ pid
    if(null != pid && "" != pid){
        sh "kill ${pid}"
    }
    sleep 5
}

def startTomcat(tomcatHome){
    println("---- startTomcat ----:"+tomcatHome)
    script{
        withEnv(['JENKINS_NODE_COOKIE=dontkillme']) {
            sh """
            ${tomcatHome}/bin/startup.sh
          """
        }
    }
}

def copyWarToTomcat(tomcatWebPath){
    println("---- COPY ----:"+tomcatWebPath)
    sh """
    cp ${params.packageName}.${params.packageType} ${tomcatWebPath}
    """
}
